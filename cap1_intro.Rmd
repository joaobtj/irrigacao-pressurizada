# Introdução à Irrigação Pressurizada {#intro}

<!-- ## Irrigação e Recursos Hídricos -->

A agricultura irrigada tem sido importante estratégia para otimização da produção mundial de alimentos, proporcionando desenvolvimento sustentável no campo, com geração de empregos e renda de forma estável. Atualmente, mais da metade da população mundial depende de alimentos produzidos em áreas irrigadas.

No passado, a utilização da irrigação era uma opção técnica de aplicação de água que visava principalmente à luta contra a seca. Atualmente, a irrigação, no foco do agronegócio, insere-se em um conceito mais amplo de agricultura irrigada, sendo uma estratégia para aumento da produção, produtividade e rentabilidade da propriedade agrícola de forma sustentável, preservando o meio ambiente e criando condições para manutenção do homem no campo, através da geração de empregos permanentes e estáveis.

A irrigação não deve ser considerada isoladamente, mas sim como parte de um conjunto de técnicas utilizadas para garantir a produção econômica de determinada cultura com adequados manejos dos recursos naturais. Portanto, devem ser levados em conta os aspectos de sistemas de plantios, de possibilidades de rotação de culturas, de proteção dos solos, de fertilidade do solo, de manejo integrado de pragas e doenças, mecanização etc., perseguindo-se a produção integrada e a melhor inserção nos mercados.

A história da irrigação se confunde com a do desenvolvimento e prosperidade econômica dos povos, em que as principais civilizações antigas tiveram sua origem em regiões áridas, onde a produção só era possível graças à irrigação.

As plantas necessitam de água para seu crescimento e desenvolvimento. A demanda de água da cultura é alcançada pela água presente no solo através do sistema radicular. 



<!-- ***Importância*** -->
<!-- ***justificar, etc*** -->



## Conceito de Irrigação

***Definição Clássica:***

>Aplicação artificial de água ao solo, em intervalos definidos e em quantidade suficiente para fornecer às espécies vegetais umidade ideal para seu pleno desenvolvimento.

***Definição Conservacionista:***

>Aplicação artificial de água ao solo, através de métodos capazes de atender da melhor forma possível as condições do meio físico (demanda de água da cultura, condições topográficas do terreno, capacidade de retenção de água do solo ...) e aos objetivos desejados (maximizar a produtividade, maximizar o lucro ...) com mínima degradação ambiental

***Definição Social:***

>Por uma perspectiva social, a irrigação deverá ser definida como uma prática agrícola capaz de maximizar os benefícios totais, incluindo os benefícios não monetários como a segurança alimentar, a geração de empregos, a melhoria das condições socioeconômicas das comunidades rurais, a fixação do homem no campo e a proteção da qualidade da água.



## Caráter

+	Obrigatório: zonas áridas (< 400 mm anuais) e semi-áridas ( 400-500 mm anuais). 
+	Complementar: regiões úmidas (> 600 mm anuais) com má distribuição temporal e espacial das chuvas.

## Objetivos

+	Objetivos financeiros - maximizar a relação benefício/custo com o aumento da produção quer em quantidade, quer em qualidade, ou incorporar à agricultura terrenos que, sem o uso da irrigação, não poderiam ser cultivados.
+	Objetivos sociais – há situações em que os aspectos sociais são mais relevantes que os financeiros (projetos públicos de desenvolvimento regional): segurança alimentar; fixação do homem ao campo, melhoria das condições socioeconômicas de comunidades rurais.

## Vantagens

+	Aumenta a produtividade das culturas (Tabela 3)
+	Melhoria na qualidade da produção (Figura 16 e Figura 17).
+	Diminuição dos riscos de quebra de safra por seca
+	Aumenta o valor da propriedade e o lucro da agricultura
+	Permite um programa de cultivos (escalonamento)
+	Permite dois ou mais cultivos por ano na mesma área
+	Permite e justifica a introdução de culturas mais nobres, minimizando o risco do investimento
+	Melhora as condições econômicas das comunidades rurais
+	Aumenta a demanda de mão-de-obra e fixa o homem no meio rural


## Desvantagens

+	Alto custo inicial de investimento
     +	Irrigação por superfície U$ 500 a 800 por ha.
     +	Irrigação por aspersão U$ 800 a 1000 por ha.
     +	Irrigação por pivô central U$ 1200 a 2000 por ha.
     +	Irrigação localizada U$ 1800 a 3000 por ha.
+	Mão de obra especializada
     +	Manutenção, operação e manejo.
+	Impactos Ambientais
     +	Alteração de ecossistemas
     +	Salinização dos solos
     +	Contaminação de recursos hídricos:
          +	Resíduos de agroquímicos
          +	Eutrofização (N,P)
          +	Assoreamento
     +	Problemas de saúde pública
     +	Esquistossomose
     +	Proliferação de mosquitos
     +	Verminoses.
+	Disponibilidade hídrica
     +	Consumo de água

## Questões clássicas da irrigação

+	**Como irrigar** – trata da seleção do método de irrigação mais adequado para atender aos objetivos almejados.
+	**Quanto irrigar** – trata da definição da quantidade de água a aplicar por irrigação para atingir os objetivos almejados.
+	**Quando irrigar** – trata da definição dos intervalos entre aplicações de água e da época de paralisação das irrigações, de forma a atingir os objetivos desejados.

A quantidade e o momento em que a água deve ser aplicada são determinantes na produtividade da cultura. Quando irrigar tem um efeito mais intenso, visto que em alguns estágios de desenvolvimento da cultura o estresse hídrico pode reduzir irreversivelmente a qualidade e produtividade das culturas. 

## Os métodos de irrigação pressurizada

+	Irrigação por aspersão: é o método de irrigação em que a água é aspergida sobre a superfície do terreno, assemelhando-se a uma chuva, por causa do fracionamento do jato d’água em gotas; 
     +	Aspersão convencional
     +	Pivô central
     +	Autopropelido

+	Microirrigação ou irrigação localizada: é o método em que a água é aplicada próxima a região radicular, com pequena intensidade e alta frequência.
     +	Gotejamento
     +	Microaspersão



