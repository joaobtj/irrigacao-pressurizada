# A Água no Solo {#solo}

O solo é constituído de partículas sólidas, líquidas e gasosas. 

A fase sólida é também chamada matriz do solo e composta por partículas minerais e matéria orgânica. 

A fase líquida é representada pela água no solo e solutos dissolvidos. É mais correto denominar essa fase como solução do solo, embora muitas vezes seja utilizada simplesmente água do solo. 

A fase gasosa contém ar e outros gases. 

Se fosse possível separar essas três fases, em um volume de solo típico, poder-se-ia visualizar algo como apresentado na Figura \@ref(fig:2-composicao). Na escala temporal de uma safra agrícola, o volume de sólidos pode ser considerado praticamente constante, enquanto os gases e a solução dividem o espaço poroso e têm variação bastante dinâmica, podendo mudar sua proporção em questão de minutos. 


```{r 2-composicao, echo=FALSE, out.width=400, fig.cap="Representação da composição volumétrica de um solo."}

knitr::include_graphics('images/2-composicao-solo.png')

```

## Relações entre massa e volume de um solo seco

Em um solo seco, considerando apenas a matriz sólida e desprezando a massa de ar presente nele, podemos definir algumas relaçõe bastante importantes.

Denomina-se **densidade do solo (D~s~)** a relação entre a massa e o volume de uma amostra de solo seco. 

\begin{equation} 
  D_s = \frac{m_s}{V_s}
  (\#eq:ds)
\end{equation} 
 
em que:

+ m~s~ - massa do solo seco
+ V~s~ - volume do solo

De forma semelhante, a relação entre a massa de solo seco e o volume de partículas sólidas é denominada **densidade das partículas do solo (D~p~)**. O valor médio da densidade das partículas de solos minerais é de aproximadamente 2,65. Este valor pode ser usado na maioria dos casos práticos.
 
\begin{equation} 
  D_p = \frac{m_s}{V_{part}}
  (\#eq:dp)
\end{equation} 

em que:

+ V~part~ - volume das partículas do solo
 
Outro importante parâmetro do solo é a **porosidade ($\alpha$)**, definida como a razão entre o volume de poros, que corresponde ao volume de solução mais o volume de gases, e o volume de solo. 

\begin{equation} 
  \alpha = \frac{V_{poro}}{V_s}
  (\#eq:porosidade2)
\end{equation} 

em que:

+ V~poro~ - volume dos poros

No entanto, é mais comum estimarmos a porosidade pela razão entre a densidade do solo e a densidade das partículas.

\begin{equation} 
  \alpha = 1 - \frac{D_s}{D_p}
  (\#eq:porosidade)
\end{equation} 
 

## Conteúdo de água no solo

O conteúdo de água no solo pode ser expresso em **base de massa**, também chamado umidade em peso (u), e definida como a razão entre a massa de água contida no solo em um determinado instante e a massa do solo seco.

\begin{equation} 
  u = \frac{m_a}{m_s}
  (\#eq:umidademassa)
\end{equation} 

em que:

+ m~a~ - massa de água

O conteúdo de água no solo também pode ser expresso em **base de volume**, também chamado umidade em volume ($\theta$) e definido como a razão entre o volume de água contido no solo em um determinado instante e o volume do solo.
 
 \begin{equation} 
  \theta = \frac{V_a}{V_s}
  (\#eq:teta)
\end{equation} 
 
em que:

+ V~a~ - volume de água

Para umidade em base massa, podem ser utilizadas as unidades g/g, kg/kg, etc., em que se considera gramas de água por grama de solo seco. Para umidade em base volume, podem ser utilizadas as unidades cm^3^/cm^3^, m^3^/m^3^, etc., em que se considera centímetro cúbico de água por centímetro cúbico de solo.

A vantagem de se trabalhar com umidade em volume é que seu valor corresponde à lâmina de água retida por camada de solo. Por exemplo, se a umidade do solo em volume é de 0,20 cm^3^/cm^3^ ou 20%, significa que em cada camada de solo de espessura X existe 0,20.X de água, ou seja, em cada centímetro de solo existe 2 mm de água.

É possível relacionar a umidade em peso com a umidade em volume, desde que se conheça a densidade do solo.

\begin{equation} 
  \theta = u \cdot D_s
  (\#eq:uteta)
\end{equation} 


A razão de saturação de um solo (S) é definida como a relação entre o volume da água e o volume total de poros. Quando o volume de poros está totalmente cheio de água, diz-se que o solo está saturado, e a razão de saturação é de 100%.
 
 \begin{equation} 
  S = \frac{V_a}{V_{poro}}
  (\#eq:saturacao)
\end{equation} 
 

:::{.example}

Retirou-se uma amostra de solo com um anel volumétrico de 10 cm de altura e 10 cm de diâmetro. A massa de solo úmido foi 980 g e de solo seco 810 g. Sendo a densidade das partículas igual a 2,65, calcular:

+	a densidade do solo
+	a umidade do solo em base de massa seca
+	a umidade do solo em base de volume
+	a porosidade total do solo
+	a razão de saturação

\n

$D_s = \frac{810}{\pi \cdot 10^2/4 \cdot 10} = 1,03  g/cm^3$
  
$u = \frac{980-810}{810} = 0,2099 g/g$
  
$\theta = 0,2099 \cdot 1,03 = 0,2165  cm^3/cm^3$
  
$\alpha = 1-\frac{1,03}{2,65} = 0,6113$  
  
$S = \frac{0,2165}{0,6113} = 0,3541$


:::

## Armazenamento da água no solo

A lâmina aplicada multiplicada pela área de interceptação da lâmina nos fornecerá o volume equivalente. Assim, define-se 1 mm de lâmina de água como sendo um volume de 1 L distribuído em uma área de 1 m^2^. Da mesma forma, 1 mm é igual a 10 m^3^/ha.

A lâmina de água armazenada (h) em um perfil solo homogêneo até uma profundidade *Z* é calculada por:

\begin{equation} 
  h = \theta \cdot Z
  (\#eq:h2)
\end{equation} 

No entanto, é mais comum encontrar perfis de solo heterogêneos. Nestes casos, o armazenamento é calculado estratificando o perfil de solo em pequenas camadas homogêneas ($\Delta$ Z):

\begin{equation} 
  h=\sum \bar{\theta} \cdot \Delta Z
  (\#eq:h)
\end{equation} 


```{r 2-armaz, echo=FALSE, out.width=400, fig.cap="Armazenamento da água no solo."}

knitr::include_graphics('images/2-armaz.png')

```

:::{.example}


Considere o perfil de umidade da tabela abaixo:
  
|     Profundidades (cm)    |Umidade em 28/01 (cm^3^/cm^3^) |Umidade em 31/01 (cm^3^/cm^3^) |
|---------------------------|---------------------------|--------------|
|     00 – 15               |     0,331                 |     0,295    |
|     15 – 30               |     0,368                 |     0,351    |
|     30 – 45               |     0,410                 |     0,393    |
|     45 – 60               |     0,484                 |     0,474    |
|     60 – 75               |     0,439                 |     0,435    |
|     75 – 90               |     0,421                 |     0,421    |
|     90 – 105              |     0,396                 |     0,422    |
|     105 – 120             |     0,370                 |     0,400    |
  
a) Determine o armazenamento de água para os dias 28/01 e 31/01 por camada e para o perfil inteiro do solo.

b) Determine o consumo de água entre esses dias.


\n

a) 

$h_{28/01} = 0,331 \cdot 150 + 0,368 \cdot 150 + ... + 0,370 \cdot 150 = 482,85 mm$

$h_{31/01} = 0,295 \cdot 150 + 0,351 \cdot 150 + ... + 0,400 \cdot 150 = 478,65 mm$
  
b)

$consumo = 482,85 - 478,65 = 4,2 mm$
  
:::


## Potencial da água no solo

A água no solo está sujeita a forças de diferentes origens e de intensidade variável, resultando no chamado potencial da água no solo. O potencial total da água no solo ($\psi$~t~) é composto dos seguintes componentes:

+	Mátrico ($\psi$~m~): resulta dos efeitos combinados dos fenômenos da capilaridade e adsorção dentro da matriz do solo quando este se apresenta não saturado.
+	Osmótico ($\psi$~s~): determinado pela presença de solutos na água.
+	Pressão ($\psi$~p~): definido pela pressão hidrostática de uma coluna de água formada em um solo saturado.
+	Gravitacional ($\psi$~g~): determinado pela posição relativa do ponto de medição.
 
A relação fundamental entre o conteúdo de água no solo e o potencial mátrico é chamada curva de retenção da água no solo. Tipicamente, a curva de retenção é descrita por equações contínuas não lineares, e como o potencial mátrico se estende por várias ordens de magnitude, ela é frequentemente plotada em escala logarítmica. São muitas as funções matemáticas que descrevem a curva de retenção, sendo mais comumente utilizada a equação de Van Genuchten:
 
 \begin{equation} 
  \frac{\theta - \theta_r}{\theta_s - \theta_r} = \left (\frac{1}{1+(\alpha \cdot |\psi_m|)^n} \right)^m
  (\#eq:vg)
\end{equation} 


Em que:

+ $\theta$ – conteúdo de água volumétrico;
+ $\theta$~s~ – conteúdo de água na saturação;
+ $\theta$~r~ – conteúdo de água residual;
+ |$\psi$~m~| - valor absoluto (sempre positivo) do potencial mátrico;
+ $\alpha$, m e n - parâmetros de ajuste dependentes do formato da curva de retenção; $\alpha$>0; n>1; 0<m<1.


```{r 2-curvaret, echo=FALSE, out.width=400, fig.cap="Curva de retenção da água no solo."}

knitr::include_graphics('images/2-curvaret.png')

```

:::{.example}


Considere a curva de retenção da figura abaixo. A capacidade de campo corresponde a um potencial mátrico de 0,1 atm. A cultura a ser irrigada é o feijoeiro (Z=50 cm) e as irrigações devem ser reiniciadas quando o potencial mátrico atingir 0,5 atm. Determinar a lâmina de água a ser aplicada para elevar a umidade até a capacidade de campo. Considerar ds=1,4.



```{r 2-curvaret-exerc, echo=FALSE, out.width=500}

knitr::include_graphics('images/2-curvaret-exerc.png')

```


\n

Analisando o gráfico:

$\theta_{CC} = 29 \%$
  
$\theta_{CR} = 18 \%$
  
$Lamina = (0,29-0,18) \cdot 1,4 \cdot 500 = 77 mm$

:::


## Disponibilidade da água no solo

A água no solo não é estática, mas dinâmica, movimentando-se em função do gradiente de  potencial entre dois pontos. A água disponível às culturas representa a quantidade de água que um solo retem em um determinado momento. Normalmente, são definidos um limite superior e um limite inferior de disponibilidade de água no solo.

O limite inferior é conhecido como **Ponto de Murcha Permanente (PMP)** e corresponde à tensão de 1500 kPa. Considera-se que as culturas não são capazes de retirar água do solo quando a umidade está abaixo do PMP. Apesar dessa definição não ser totalmente verdadeira, pois há várias exceções, continua sendo uma boa metodologia, ao menos para a agricultura irrigada.

O limite superior é chamado de **Capacidade de Campo (CC)**. A tensão correspondente a CC é muito variável, em função da cultura, tipo de solo e demanda da atmosfera. Valores comuns estão entre 10 e 30 kPa. Considera-se que as culturas não conseguem aproveitar a água do solo retida acima da CC pois esta drena muito rapidamente. Assim como para o PMP, esta é uma definição muito simplificada, mas que atende muitas situações na agricultura irrigada.



```{r 2-reserv, echo=FALSE, out.width=400, fig.cap="O solo pode ser considerado um reservatório de água."}

knitr::include_graphics('images/2-reserv.png')

```

A água disponível de um solo pode ser facilmente calculada, desde que se conheçam os teores de umidade correspondente ao limite superior (CC) e ao limite inferior (PMP). No caso da agricultura irrigada, também são imprescindíveis informações da cultura, como a profundidade efetiva do sistema radicular e sensibilidade ao estresse hídrico.

A **disponibilidade total de água no solo (DTA)** é uma característica do solo, que corresponde à água armazenada no intervalo entre os limites inferior e superior. Pode ser expressa em altura de lâmina de água por profundidade de solo, milímetro de água por milímetro de solo ou, mais comum, milímetro de água por centímetro de solo (multiplicar por 10 na Equação \@ref(eq:dta)).

\begin{equation} 
  DTA = \theta_{CC} - \theta_{PMP}
  (\#eq:dta)
\end{equation} 


```{r tab-dta, echo=FALSE}
tex <- c("Grossa", "Média", "Fina")
dta <- c("0,4 a 0,8","0,8 a 1,6","1,2 a 2,4")
tab <- data.frame(tex,dta)
colnames(tab) <- c("Textura","DTA (mm/cm)")

knitr::kable(tab, booktabs=TRUE, align="cc",
             caption="Limites de DTA para solos de diferentes texturas.")
```

A **capacidade total de água no solo (CTA)** corresponde à disponibilidade de água até uma determinada profundidade. Também é conhecida como capacidade de água disponível (CAD) ou simplesmente água disponível (AD). Podemos considerar a profundidade do perfil do solo, mas é mais comum considerar a profundidade efetiva do sistema radicular (Z), ou seja, a profundidade que contém pelo menos 80% do sistema radicular.
 
\begin{equation} 
  CTA = DTA \cdot Z
  (\#eq:cta)
\end{equation} 

```{r tab-z, echo=FALSE}

c1 <- c("Abacate","Abacaxi","Abóbora","Alcachofra","Alface","Alfafa","Algodão","Alho","Amendoim","Arroz","Aspargo","Aveia","Banana","Batata","Batata-doce","Berinjela")
z1 <- c("60 – 90","20 – 40",50,70,"20 – 30",60,60,"20 - 30",30,"30 - 40","120 - 160",40,40,"25 - 60","50 - 100",50)

c2 <- c("Beterraba","Café","Café","Cana-de-açúcar","Cebola","Cenoura","Couve","Couve-flor","Ervilha","Espinafre","Feijão","Laranja","Linho","Maçã","Mangueira","Melancia")
z2 <- c(40,50,"40 - 60",40,"20 - 40","35 - 60","25 - 50","25 - 50","50 - 70","40 - 70",40,60,20,60,60,"40 - 50")

c3 <- c("Melão","Milho","Morango","Nabo","Pastagem","Pepino","Pêssego","Pimenta","Pimentão","Rabanete","Soja","Tabaco","Tomate","Trigo","Vagem","Videira")

z3 <- c("30 - 50",40,"20 - 30","55 - 80",30,"35 - 50",60,50,"30 - 70","20 - 30","30 - 40",30,40,"30 - 40",40,60)


tab <- data.frame(c1,z1,c2,z2,c3,z3)
colnames(tab) <- rep(c("Cultura","Z (cm)"),3)

knitr::kable(tab, booktabs=TRUE, align="c",
             caption="Profundidade efetiva do sistema radicular (Z) de algumas culturas no estágio de máximo desenvolvimento vegetativo.")
```


A **capacidade real de água no solo (CRA)** é definida como a fração da capacidade total de água no solo que a cultura poderá utilizar sem afetar significativamente a sua produtividade. Também é conhecida como água facilmente disponível (AFD).

\begin{equation} 
  CRA = CTA \cdot f
  (\#eq:cra)
\end{equation} 
 
O fator de disponibilidade f varia entre 0,2 e 0,8. Os valores menores são usados em culturas mais sensíveis ao déficit hídrico e os maiores nas culturas mais resistentes.

```{r tab-dra, echo=FALSE}
cult <- c("Verduras e Legumes", "Frutas e Forrageiras", "Grãos e Algodão")
dra <- c("0,2 a 0,6","0,3 a 0,7","0,4 a 0,8")
tab <- data.frame(cult,dra)
colnames(tab) <- c("Grupo de culturas","f")

knitr::kable(tab, booktabs=TRUE, align="cc",
             caption="Fator de disponibilidade de água no solo.")
```

De uma forma um pouco diferente, a CRA também pode ser entendida como a quantidade de água disponível no solo entre a capacidade de campo e a umidade crítica inferior (UC) a que essa cultura pode ser submetida.

\begin{equation} 
  CRA = (\theta_{CC} - \theta_{UC}) \cdot Z 
  (\#eq:cra2)
\end{equation} 

ou então 

\begin{equation} 
  CRA = (\theta_{CC} - \theta_{PMP}) \cdot Z \cdot f
  (\#eq:cra3)
\end{equation} 

:::{.example}


Calcular DTA, CTA e CRA para a seguinte condição:
  
+	CC = 32% em peso
+	PMP = 18% em peso
+	D~s~ = 1,2
+	cultura milho Z = 50 cm
+	f = 0,5




\n

$DTA = (0,32 - 0,18) \cdot 1,2 \cdot 10 = 1,68 mm/cm$

$CTA = 1,68 \cdot 50 = 84 mm$

$CRA = 84 \cdot 0,5 = 42 mm$

:::

 



