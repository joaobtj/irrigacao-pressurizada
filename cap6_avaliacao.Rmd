# Avaliação de Sistemas de Irrigação Pressurizada {#avaliacao}

A valiação de sistemas de irrigação pode ter como objetivo:

* avaliar os efeitos da uniformidade na quantificação da lâmina de irrigação;
* verificar a necessidade de troca de componentes;
* avaliar a gestão dos recursos hídricos;
* avaliar o retorno financeiro sobre o investimento;

As medidas de avaliação estão relacionada à eficiência e uniformidade

## Eficiência

A eficiência da irrigação pode ser definida como a razão entre a água consumida pelo processo de evapotranspiração e a água retirada para a irrigação.

A água não aproveitada pode ser considerada como perda. No entanto, este conceito deve ser aplicado em uma escala adequada, mais comumente, de parcela irrigada.


De modo geral, a eficiência da irrigação pode ser particionada em eficiência de aplicação e eficiência de distribuição.

### Eficiência de aplicação

É a fração da água aplicada que permaneceu disponível para infiltrar no solo.

$E_a = \frac{L_i}{L_a}$

em que:

* $L_i$ - lâmina de água infiltrada
* $L_a$ - lâmina de água aplicada

A perda por evaporação e deriva é igual a 1-E~a~.



### Eficiência de distribuição

É a fração de água infiltrada que permaneceu disponível na zona radicular da cultura.

$E_d = \frac{L_s}{L_i}$

em que:

* $L_s$ - lâmina de água útil armazenada
* $L_i$ - lâmina de água infiltrada

A perda por percolação é igual a 1-E~d~.


### Eficiência de irrigação

É a fração da água aplicada que é armazenada no perfil do solo e disponível para a cultura.

$E_a = \frac{L_s}{L_a}$

em que:

* $L_s$ - lâmina de água útil armazenada
* $L_a$ - lâmina de água aplicada



## Uniformidade

A medida de uniformidade expressa a variabilidade da lâmina de irrigação aplicada no solo. 
Um sistema de irrigação que não aplica água com uniformidade irá aplicar água em excesso em algumas áreas enquanto que em outras áreas irá aplicar água em déficit.

Muitas fórmulas matemáticas são utilizadas para descrever a uniformidade de um sistema de irrigação. O coeficiente de uniformidade de Christiansen (CUC) é o mais utilizado, especialmente para sistemas de irrigação por aspersão, 

A fórmula utiliza o desvio média absoluto como medida de dispersão:

$$
CUC_\% = \left(1 - \frac{\sum|X_i - \overline X|}{\sum X_i}\right)\cdot 100
$$

em que:

* $X_i$ - lâmina aplicada no ponto *i*
* $\overline X$ - lâmina média aplicada



Em geral, valores acima de 85% são aceitáveis para sistemas de aspersão. Valores abaixo deste podem ser admitidos caso a precipitação seja relevantes durante o ciclo da cultura.

Os valores da lâmina aplicada são obtidos em ensaios como o demonstrado na Figura  \@ref(fig:6-unif).

```{r 6-unif, echo=FALSE, out.width=400, fig.cap="Ensaio para determinação da uniformidade em sistemas de aspersão."}

knitr::include_graphics('images/6-unif.png')

```

Para pivô central, a lâmina em cada ponto deve ser ponderada pois a área que cada coletor representa é maior a partir do ponto central do pivô. A equaçao fica assim:

$$
CUC_\% = \left(1 - \frac{\sum ( S_i \cdot |X_i - \overline X|)}{\sum(X_i \cdot S_i)} \right) \cdot 100 
$$
A lâmina média é calculada pela seguine equação:

$$
\overline X = \frac{\sum X_i \cdot S_i}{\sum S_i}
$$

em que:

* $X_i$ - lâmina aplicada no ponto *i*
* $\overline X$ - lâmina média aplicada
* $S_i$ - distância do coletor ao ponto do pivô



```{r 6-unif-pivo, echo=FALSE, out.width=400, fig.cap="Ensaio para determinação da uniformidade em pivô central."}

knitr::include_graphics('http://secure.caes.uga.edu/extension/publications/files/html/C911/images/C911-1.jpg')

```


No caso dos coletores estarem dipostos em espaçamentos equidistantes, a distância do coletor ao ponto do pivô pode ser substituída pelo número de ordem do coletor.


:::{.example}

Calcular o CUC com os dados abaixo:
  

![](images/6-cuc.png)




```{r, echo=FALSE}
library(magrittr)

x <- c(16.9, 21.8, 22.0, 21.6, 16.6, 15.3,
       14.9, 20.8, 21.8, 19.9, 17.4, 11.8,
       16.6, 20.4, 23.0, 20.1, 15.8, 19.9,
       17.9, 21.3, 23.8, 22.4, 18.1, 17.6)

xm <- mean(x) %>% round(2)

CD <- abs(x-xm) %>% sum() %>% divide_by(sum(x)) 

CUC <- ((1-CD)*100) %>% round(2)



```



\n


$\overline X = \frac{16,9 + 21,8 + ... + 17,6}{24} = 19,07$

$CUC_\% = \left(1 - \frac{|21,8-19,07| + |16,9-19,07| + ... + |17,6-19,07|}{16,9+21,8+...+17,6}\right)\cdot 100 = 86,51\%$

:::